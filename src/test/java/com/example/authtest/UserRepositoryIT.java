package com.example.authtest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class UserRepositoryIT {

    @Autowired
    private UserRepository userRepository;

    @Test
    void newUserIsPersisted() {
        var newUser = new UserEntity();
        newUser.setUsername("username");
        newUser.setPassword("password");

        var userId = userRepository.save(newUser).getId();
        var user = userRepository.findById(userId).orElseThrow();

        assertThat(user.getUsername()).isEqualTo("username");
        assertThat(user.getPassword()).isEqualTo("password");
    }
}
