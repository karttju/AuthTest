package com.example.authtest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.ArrayList;
import java.util.Date;

@Component
public class JwtTokenUtil {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);
    private static final long EXPIRE_DURATION = 60 * 60 * 1000L;

    Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public String generateAccessToken(User user) {
        var roles = new ArrayList<String>();

        for (GrantedAuthority authority: user.getAuthorities()) {
            roles.add(authority.getAuthority());
        }

        return Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuer("JIK")
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_DURATION))
                .claim("roles", roles)
                .signWith(key)
                .compact();
    }

    public Jws<Claims> validateJwtToken(String authToken) {
        try {
            return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(authToken);
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return null;
    }

    public String getUserNameFromClaims(Jws<Claims> claims) {
        return claims.getBody().getSubject();
    }

    public ArrayList<?> getRolesFromClaims(Jws<Claims> claims) {
        return claims.getBody().get("roles", ArrayList.class);
    }
}
