package com.example.authtest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class Controller {

    @GetMapping("/view")
    @PreAuthorize("hasRole('USER')")
    public String returnView() {
        return "You have accessed normal view";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String returnAdmin() {
        return "You have accessed admin view";
    }
}
