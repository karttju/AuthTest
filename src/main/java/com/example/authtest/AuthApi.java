package com.example.authtest;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Getter
@AllArgsConstructor
class Request {
    private String username;
    private String password;
}

@Getter
@AllArgsConstructor
class Response {
    private String token;
}

@CrossOrigin
@RestController
public class AuthApi {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @PostMapping("/login")
    public ResponseEntity<Response> login(@RequestBody @Valid Request request) {
        var authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(), request.getPassword()
                )
        );

        var user = (User) authentication.getPrincipal();
        var accessToken = jwtTokenUtil.generateAccessToken(user);
        var response = new Response(accessToken);

        return ResponseEntity.ok().body(response);
    }
}
